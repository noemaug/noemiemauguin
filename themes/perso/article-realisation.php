<?php include(dirname(__FILE__).'/header.php'); ?>
<?php include(dirname(__FILE__).'/sidebar.php'); ?>

<article role="article" class="post-<?php echo $plxShow->artId(); ?>">
<p class="artPrevNext"><?php $plxShow->artPrevNext('<', '>', ' autre réalisation ') ?></p>

	<h2 class="cRandom">
		<?php $plxShow->artTitle(''); ?>
	</h2>
	
	<div class="flex-container">
	
		<div class="text-article w40">
			<div class="mbm">
			 	<?php $plxShow->artContent(); ?>
			 </div>
	
	
			<?php 
				$github = $plxShow->callHook('champArt', 'github_R');
				if(!empty($github)):
			?>
				<a title="lien vers le projet sur Github" class="more-link" href="<?php eval($plxShow->callHook('champArt', 'github')); ?>">
					<?php include(dirname(__FILE__).'/img/github.svg'); ?>
					<span class="link-text">retrouvez le projet sur github</span>
				</a>				
				<?php endif; ?>
			<br>
			<?php 
				$website = $plxShow->callHook('champArt', 'website_R');
				if(!empty($website)):
			?>
				<a title="lien vers le site" class="more-link" href="<?php eval($plxShow->callHook('champArt', 'website')); ?>">
					<?php include(dirname(__FILE__).'/img/website.svg'); ?>
					<span class="link-text">visiter le site</span>
				</a>				
				<?php endif; ?>
	
	
		</div>
	
		<div class="galerie w60">
			<?php eval($plxShow->callHook("ArtgalerieDisplay"));?>	
		</div>
	</div>
	
<p class="artPrevNext"><?php $plxShow->artPrevNext('<', '>', ' autre réalisation ') ?></p>


</article>

<?php include(dirname(__FILE__).'/footer.php'); ?>
