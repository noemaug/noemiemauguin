<?php include(dirname(__FILE__).'/header.php'); ?>
<?php include(dirname(__FILE__).'/sidebar.php'); ?>


<article role="article" class="post-<?php echo $plxShow->artId(); ?>">

<p class="artPrevNext"><?php $plxShow->artPrevNext('<', '>', ' autre article ') ?></p>

	<h2 class="cRandom">
		<?php $plxShow->artTitle(''); ?>
	</h2>

	<div class="flex-container">
	
		<div class="text-article w40">
			<div class="mbm">
			 	<?php $plxShow->artContent(); ?>
			 </div>

			<?php 
				$website = $plxShow->callHook('champArt', 'website_R');
				if(!empty($github)):
			?>
				<a title="lire la suite" class="more-link" href="<?php eval($plxShow->callHook('champArt', 'website')); ?>">
					<div class="link-icon inbl website-icon"></div>
					<span class="link-text">visiter le site</span>
				</a>				
			<?php endif; ?>


		</div>

		<div class="galerie w60">
			<?php eval($plxShow->callHook("ArtgalerieDisplay")); ?>	
		</div>
	</div>

<div class="artPrevNext"><?php $plxShow->artPrevNext('<', '>', ' autre article ') ?></div>
</article>

<section role="comment">
	<?php include(dirname(__FILE__).'/commentaires.php'); ?>
</section>


<?php include(dirname(__FILE__).'/footer.php'); ?>
