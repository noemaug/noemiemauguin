<?php include(dirname(__FILE__).'/header.php'); ?>
<?php include(dirname(__FILE__).'/sidebar.php'); ?>

<section role="contact">
	<div class="bg-contact tiny-hidden"></div>
	<div class="content">
		<h2 class="cRandom">Envoyez-moi un hello !</h2>
		<?php $plxShow->staticContent(); ?>
	</div>
</section>

<div class="tiny-hidden">	<?php include(dirname(__FILE__).'/footer.php'); ?> </div>