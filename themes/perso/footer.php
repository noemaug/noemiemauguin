<?php if (!defined('PLX_ROOT')) exit; ?>
		
		<div class="marge-footer"></div>

		<footer role="contentinfo">

			<p>
				<span>Noémie Mauguin © 2015 - Conceptrice Web, qui baigne dans le code et les pixels</span>
				- 
				<span>
					ce site web utilise : 
					<a title="lien vers PluXml" href="http://www.pluxml.org">PluXml</a> - 
					<a title="lien vers Knacss" href="http://knacss.com">Knacss</a> - 
					<a title="lien vers Sass" href="http://sass-lang.com/">Sass</a>
				</span>
			</p>

		</footer>

	</section> <!-- end section main  -->

<script type="text/javascript" src="<?php $plxShow->template(); ?>/js/script.js"></script>


</body>

</html>
