<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/style/stylesheets/knacss/knacss.css" media="screen"/>
	<link rel="stylesheet" href="<?php $plxShow->template(); ?>/style/stylesheets/style.css" media="screen"/>
	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
	<link href='http://fonts.googleapis.com/css?family=Amaranth:400italic,400|Roboto+Condensed:400,700,400italic|Roboto:700|Raleway:900' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
	<script src="<?php $plxShow->template(); ?>/js/html5ie.js"></script>
	<script src="<?php $plxShow->template(); ?>/js/respond.min.js"></script>
	<![endif]-->
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
 	<script>document.write('<script src="http://' + (location.host || '${1:localhost}').split(':')[0] + ':${2:35729}/livereload.js?snipver=1"></' + 'script>')</script>


</script>
</head>

<!-- 
body 
	> cadre
	> sidebar (header + nav)
	> section main 
		> grille-vignette
		ou 
		> article
		ou 
		> about
		ou 
		> contact
	
-->

<body class="theme1">

	<div class="cadre c-top bgRandom"></div>
	<div class="cadre c-bottom bgRandom"></div>
	<div class="cadre c-left bgRandom"></div>
	<div class="cadre c-right bgRandom"></div>


