<?php include(dirname(__FILE__).'/header.php'); ?>

<section class="home">

		<img class="groundHome" alt="sol de randonnée" src="<?php $plxShow->template() ?>/img/groundHome.png">
		

	<div class="content" role="main">	
		<a title="lien vers page de réalisation" href="<?php $plxShow->racine() ?>realisations" class="logoHome">
			 <?php include(dirname(__FILE__).'/img/logoHome_walk.svg'); ?>
		</a>

		<a title="lien vers page de réalisation" class="bloc-l" href="<?php $plxShow->racine() ?>realisations">
			Dans mon sac à dos <br>
			il y a du code, <br>
			du graphisme et
			<span class="more bgRandom">+</span>     
		</a>

		<a title="lien vers page de réalisation" class="bloc-r" href="<?php $plxShow->racine() ?>realisations">
			Noémie Mauguin <br>
			<p class="cRandom">Conception Web</p>     
		</a>

		<div class="flecheOne "> &gt; </div>
		<div class="flecheTwo"> &lt; </div>
	</div>



<div class="mlm mrm tiny-hidden"><?php include(dirname(__FILE__).'/footer.php'); ?></div>
