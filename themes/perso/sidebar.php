<?php if(!defined('PLX_ROOT')) exit; ?>


<aside role="complementary">

	<a title="lien vers la page d'accueil" role="banner" class="mainLogo tiny-hidden " href="<?php $plxShow->racine() ?>">
		 <?php include(dirname(__FILE__).'/img/logo_sidebar_move.svg'); ?>
	</a>

	<div class="content">
		<a title="lien vers la page d'accueil" role="banner" class="header-title" href="<?php $plxShow->racine() ?>">
			<h1 class="name">Noémie Mauguin</h1>
			<h2 class="subTitle cRandom">Conception Web</h2>
		</a>

		<nav role="navigation" class="txtcenter">
			<input type="checkbox" id="toggle-nav">
			<label class="menu" for="toggle-nav" onclick><span>menu</span></label>
			<ul class="menu">
			<?php $plxShow->staticList(
				'', 
				'<li id="#static_id" class="#static_class">
					<a href="#static_url" class="#static_status" title="#static_name">#static_name</a>
				</li>'
			) ?>	
			</ul>
		</nav>

		<div class="flex-container wrap-social">
			<input type="checkbox" id="toggle-social">
			<label class="menu" for="toggle-social" onclick><span>social</span></label>
			<ul class="social">
				<li><a title="lien vers le profil Github" href="http://github.com/noemaug"></a></li>
				<li><a title="lien vers le profil Twitter" href="http://twitter.com/noemaug"></a></li>
				<li><a title="lien vers le profil Linkedin" href="http://www.linkedin.com/in/noemaug"></a></li>
				<li><a title="lien vers la playlist spotify" href="http://play.spotify.com/user/noemaug/playlist/212YpOLGtQGQ7rUgoewQvJ"></a></li>
			</ul>
		</div>
	</div>

</aside>

<section class="main">
