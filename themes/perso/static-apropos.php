<?php include(dirname(__FILE__).'/header.php'); ?>
<?php include(dirname(__FILE__).'/sidebar.php'); ?>

<h2 class="cRandom">Chemins parcourus ...</h2>

<div class="grid-about parcours"> <!-- parent div (ou autre) de 3-1 colonnes -->

    <div>2016</div>
    <div><p class="cRandom">Infra Creative Workshop</p>Bischheim</div>
    <div>Développeuse Front-end pour des sites e-commerce utilisant Prestashop.</div>
    
    <div>2015</div>
    <div><p class="cRandom">3W Academy</p>Strasbourg</div>
    <div>Formation de développeur web. Apprentissage des langages : HTML, CSS, Javascript, MySQL, PHP. 
        <br> 10% de théorie et 90% de pratique  </div>
    
    <div>2014</div>
    <div><p class="cRandom">Auto Entrepreneur</p>Autriche</div>
    <div>
        Intégratrice, Webmaster et infographiste en collaboration avec <a title="lien vers RayFomation" href="http://www.ray.fr">Ray Formation</a>. Création du site internet du <a title="lien vers Haxahus" href="http://haxahus.org/">musée des sorcières</a>, plaquette d’information pour le centre de natation de Vénissieux.
        <br>Travail à distance tout en expérimentant la vie à l’étranger, l'apprentissage de l'allemand ainsi que le partage humain et de connaissance par le <a title="lien vers wiki wwoofing" href="http://www.wikiwand.com/fr/WWOOF">wwoofing</a>.
    </div>
    
    <div>2013</div>
    <div><p class="cRandom">Patte Blanche</p>Montpellier</div>
    <div>Stage dans l'agence de communication & conseil développement durable à Montpellier.
        Participation à la réalisation d’une courte vidéo pour le cabinet d’avocat Fidal. Collaboration à la conception de brochures pour les différents programmes d’Unis-Cité.</div> 
      
    <div>2013</div>
    <div><p class="cRandom">Master Conception et Intégration Multimédia</p>Lyon</div>
    <div>Formation de chef de projet multimédia s'équilibrant entre la gestion de projet, la communication, la programmation et la direction artistique. </div>
    
    <div>2012</div>
    <div><p class="cRandom">Ray Formation</p>Lyon</div>
    <div> Centre de formations dédiées aux architectes. 
        Création d'une nouvelle charte graphique et refonte du site internet.</div>
</div>

<br><br>

<h2 class="cRandom">Dans mon sac à dos ...</h2>

<div class="skill">
    <div class="grid-sacAdos">
        <div><img alt="HTML" src="<?php $plxShow->template()?>/img/html.jpg"></div>
        <div><img alt="CSS" src="<?php $plxShow->template()?>/img/css.jpg"></div>
        <div><img alt="PHP" src="<?php $plxShow->template()?>/img/php.jpg"></div>
        <div><img alt="jQuery" src="<?php $plxShow->template()?>/img/jquery.jpg"></div>
        <div><img alt="Wordpress" src="<?php $plxShow->template()?>/img/wordpress.jpg"></div>
        <div><img alt="Prestashop" src="<?php $plxShow->template()?>/img/logo-prestashop.jpg"></div>    
        <div><img alt="Sass" src="<?php $plxShow->template()?>/img/sass.jpg"></div>
        <div><img alt="Linux" src="<?php $plxShow->template()?>/img/linux.jpg"></div>
        <div><img alt="Photoshop" src="<?php $plxShow->template()?>/img/photoshop.jpg"></div>
        <div><img alt="Illustrator" src="<?php $plxShow->template()?>/img/illustrator.jpg"></div>
        <div><img alt="inDesign" src="<?php $plxShow->template()?>/img/indesign.jpg"></div>
        <div><img alt="After Effect" src="<?php $plxShow->template()?>/img/aftereffect.jpg"></div>
    </div>
</div>


<p class="big"><b class="cRandom">Vous recherchez une compétence qui ne se trouve pas là ?</b> Un framework, un CMS ?<br> 
Pas de soucis,<b> je m'adapte rapidement </b>et je serai ravis d'ajouter un outil dans mon sac à dos
</p>

<br><br>

<h2 class="cRandom">Mais aussi ...</h2>


<div class="grid-loisir">
    <div class="loisir violon"><?php include(dirname(__FILE__).'/img/violon.svg'); ?></div> 
    <div class="loisir velo"><?php include(dirname(__FILE__).'/img/velo.svg'); ?></div> 
    <div class="loisir abeille"><?php include(dirname(__FILE__).'/img/abeille.svg'); ?></div> 
</div>


<?php include(dirname(__FILE__).'/footer.php'); ?>
