<?php 

if(!defined('PLX_ROOT')) exit;

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# nombre de champs existants
$nbchamp = floor(sizeof($plxPlugin->getParams())/4);

if(!empty($_POST)) {

	if (!empty($_POST['label-new']) AND !empty($_POST['champ-new']) AND !empty($_POST['type-new'])) {
        # création d'un nouveau champ
        $newchamp = $nbchamp + 1;
		$plxPlugin->setParam('label'.$newchamp, plxUtils::strCheck($_POST['label-new']), 'string');
        # on teste voir si le "raccourci" n'existe pas déjà
        for($i=1; $i<=$nbchamp; $i++) {
            if ($plxPlugin->getParam('champ'.$i)==str_replace("-","_",plxUtils::title2url($_POST['champ-new']))) {
                $champ_new = str_replace("-","_",plxUtils::title2url($_POST['champ-new'].'1'));
            }else{
                $champ_new = str_replace("-","_",plxUtils::title2url($_POST['champ-new']));
            }
        } 
		$plxPlugin->setParam('champ'.$newchamp, $champ_new, 'string');
		$plxPlugin->setParam('type'.$newchamp, plxUtils::strCheck($_POST['type-new']), 'string');
		if (empty($_POST['groupe-new'])) { // si le champ groupe n'est pas renseigné -> on crée le groupe "divers"
			$plxPlugin->setParam('groupe'.$newchamp, $plxPlugin->getLang('L_OTHER'), 'string');
		} else {
			$plxPlugin->setParam('groupe'.$newchamp, plxUtils::strCheck($_POST['groupe-new']), 'string');
		}
        $plxPlugin->saveParams();
        
	}else{
        
        # Mise à jour des champs existants
        for($i=1; $i<=$nbchamp; $i++) {
            if($_POST['delete'.$i] != "1" AND !empty($_POST['label'.$i]) AND !empty($_POST['champ'.$i]) AND !empty($_POST['type'.$i])){ // si on ne supprime pas et que les champs ne sont pas vide
                
                #mise a jour du label
                $plxPlugin->setParam('label'.$i, plxUtils::strCheck($_POST['label'.$i]), 'string');
                
                # on teste voir si la balise n'existe pas déjà
                $champ_new = str_replace("-","_",plxUtils::title2url($_POST['champ'.$i]));
                for($j=1; $j<=$nbchamp; $j++) {
                    if ($j != $i && $plxPlugin->getParam('champ'.$j)==str_replace("-","_",plxUtils::title2url($_POST['champ'.$i]))) {
                        $champ_new = str_replace("-","_",plxUtils::title2url($_POST['champ'.$i].''.$i));
                    }
                }            
                $plxPlugin->setParam('champ'.$i, $champ_new, 'string');
                $plxPlugin->setParam('type'.$i, plxUtils::strCheck($_POST['type'.$i]), 'string');
                
                #si le groupe est vide, on le place dans "divers"
                if (empty($_POST['groupe'.$i])) {
			         $plxPlugin->setParam('groupe'.$i, $plxPlugin->getLang('L_OTHER'), 'string');
		          } else {
			         $plxPlugin->setParam('groupe'.$i, plxUtils::strCheck($_POST['groupe'.$i]), 'string');
		          }
                $plxPlugin->saveParams();
            
            }elseif($_POST['delete'.$i] == "1"){
                #on vide le champ s'il la case "supprimer" est cochée
                $plxPlugin->setParam('label'.$i, '', '');
                $plxPlugin->setParam('champ'.$i, '', '');
                $plxPlugin->setParam('type'.$i, '', '');
                $plxPlugin->setParam('groupe'.$i, '', '');
                $plxPlugin->saveParams();
            }
        }
    }
}

# mise à jour du nombre de champs existants
$nbchamp = floor(sizeof($plxPlugin->getParams())/4);
?>
<div id="champart">
<h2><?php $plxPlugin->lang('L_TITLE') ?></h2>
<p><?php $plxPlugin->lang('L_DESCRIPTION') ?></p>

<!-- champs déja créés -->
<form action="parametres_plugin.php?p=champArt" method="post" class="champart">
<fieldset>
<table class="champart table">
	<thead>
		<tr>
			<th><?php $plxPlugin->lang('L_ID') ?></th>
			<th><?php $plxPlugin->lang('L_LABEL') ?></th>
			<th><?php $plxPlugin->lang('L_CHAMP') ?></th>
			<th><?php $plxPlugin->lang('L_TYPE') ?></th>
			<th><?php $plxPlugin->lang('L_GROUPE') ?></th>
            <th><?php $plxPlugin->lang('L_DELETE') ?></th>
		</tr>
	</thead>
	<tbody>
		<?php for($i=1; $i<=$nbchamp; $i++) : ?>
        <?php $label = $plxPlugin->getParam(label.$i);
                if(!empty($label)) : ?>
		<tr class="line-<?php echo $i%2 ?>">
			<td>
				<?php echo $i; ?>
			</td>
			<td>
				<input type="text" name="label<?php echo $i; ?>" value="<?php echo plxUtils::strCheck($plxPlugin->getParam(label.$i)) ?>" />
			</td>
			<td>
				<input type="text" name="champ<?php echo $i; ?>" value="<?php echo plxUtils::strCheck($plxPlugin->getParam(champ.$i)) ?>" />
			</td>
			<td>
				<label for="ligne<?php echo $i; ?>"><?php $plxPlugin->lang('L_LIGNE') ?></label><input type="radio" name="type<?php echo $i; ?>" value="ligne" id="ligne<?php echo $i; ?>" <?php if($plxPlugin->getParam(type.$i)=='ligne'){echo "checked";}?>>
				<label for="bloc<?php echo $i; ?>"><?php $plxPlugin->lang('L_BLOC') ?></label><input type="radio" name="type<?php echo $i; ?>" value="bloc" id="bloc<?php echo $i; ?>" <?php if($plxPlugin->getParam(type.$i)=='bloc'){echo "checked";}?>>
			</td>
			<td>
				<input type="text" name="groupe<?php echo $i; ?>" value="<?php echo plxUtils::strCheck($plxPlugin->getParam(groupe.$i)) ?>" />
			</td>
            <td><input type="checkbox" name="delete<?php echo $i; ?>" value="1"></td>
		</tr>
        <?php endif; ?>
		<?php endfor; ?>
	</tbody>
</table>
</fieldset>
<?php echo plxToken::getTokenPostMethod() ?>
<p class="center">
<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_UPDATE') ?>" />
</p>
</form>

<!-- nouveau champ -->
<form action="parametres_plugin.php?p=champArt" method="post" class="champart">
<fieldset>
<table class="champart table">
    <thead>
		<tr>
			<th><?php $plxPlugin->lang('L_ID') ?></th>
			<th><?php $plxPlugin->lang('L_LABEL') ?></th>
			<th><?php $plxPlugin->lang('L_CHAMP') ?></th>
			<th><?php $plxPlugin->lang('L_TYPE') ?></th>
			<th><?php $plxPlugin->lang('L_GROUPE') ?></th>
		</tr>
	</thead>
    <tbody>
	   <tr class="new">
			<td>
				<?php $plxPlugin->lang('L_NEW') ?>
			</td>
			<td>
				<input type="text" name="label-new" value="" />
			</td>
			<td>
				<input type="text" name="champ-new" value="" />
			</td>
			<td>
				<label for="ligne-new"><?php $plxPlugin->lang('L_LIGNE') ?></label><input type="radio" name="type-new" value="ligne" id="ligne-new" checked>
				<label for="bloc-new"><?php $plxPlugin->lang('L_BLOC') ?></label><input type="radio" name="type-new" value="bloc" id="bloc-new">
			</td>
			<td>
				<input type="text" name="groupe-new" value="" />
			</td>
		</tr>
	</tbody>
</table>
</fieldset>
<?php echo plxToken::getTokenPostMethod() ?>
<p class="center">
<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_SAVE') ?>" />
</p>
</form>

</div>