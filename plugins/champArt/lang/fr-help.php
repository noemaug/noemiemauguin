<?php if(!defined('PLX_ROOT')) exit; ?>
<br>
<h2>Aide du plugin champArt</h2>
<h3>Coté rédacteur</h3>
<br>
<p>Rien de plus simple, on suit ce que nous a indiqué l'admin dans l'intitulé. Les données s'enregistrent avec l'article.</p>
<div id="champart">
<fieldset class="article">
<legend>Nom du Groupe</legend>
<p><label for="id_champArt_monchamp">Intitulé du champ personalisé:</label>&nbsp;&nbsp;<a id="toggler_monchamp" href="javascript:void(0)" onclick="toggleDiv('toggle_monchamp', 'toggler_monchamp', 'afficher','masquer')">masquer</a>
</p>
<div id="toggle_monchamp" style="display: block; ">
<input id="id_champArt_monchamp" name="champArt_monchamp" type="text" value="Valeur enregistrée dans le champ." size="66" maxlength="255">
</div>

<p><label for="id_champArt_monchamp">ex: "Téléphone portable":</label>&nbsp;&nbsp;<a id="toggler_monchamp2" href="javascript:void(0)" onclick="toggleDiv('toggle_monchamp2', 'toggler_monchamp2', 'afficher','masquer')">masquer</a>
</p>
<div id="toggle_monchamp2" style="display: block; ">
<input id="id_champArt_monchamp" name="champArt_monchamp" type="text" value="écrire ici.." size="66" maxlength="255">
</div>
</fieldset>
<br>
<h3>Cot&eacute; administrateur</h3>
<br>
<p>
Pour afficher sur le site la valeur de votre champ, il faut récupérer la <b>balise</b> du champ depuis l'interface de configuration, puis inc&eacute;rer ce code dans les <i>fichiers du th&egrave;me</i> à l'endroit d&eacute;sir&eacute;:
<blockquote><?php echo htmlentities("<?php eval(\$plxShow->callHook('champArt', 'balise')); ?>"); ?></blockquote>
</p>

<p>
Vous pouvez aussi r&eacute;cup&eacute;rer cette valeur sans l'afficher ( utile pour faire des tests etc ... ), pour cela rajoutez <b>_R</b> &agrave; la fin du param&egrave;tre:
ex: <blockquote><?php echo htmlentities("<?php \$monchamp = \$plxShow->callHook('champArt', 'balise_R')); ?>"); ?></blockquote>
</p>

<p>Un exemple d'application: <i>"si la valeur existe, j'affiche du texte"</i>:
<blockquote>
<?php echo htmlentities("<?php"); ?><br>
<?php echo htmlentities("\$monchamp = \$plxShow->callHook('champArt', 'balise_R'));"); ?><br>
<?php echo htmlentities("if(!empty(\$monchamp)) {"); ?><br>
<?php echo htmlentities("echo \"il existe une valeur\";"); ?><br>
<?php echo htmlentities("}"); ?><br>
<?php echo htmlentities("?>"); ?><br>
</blockquote>

<p>
Enfin, si vous souhaitez afficher votre valeur pr&eacute;c&eacute;d&eacute;e de son label, rajoutez <b>_L</b> &agrave; la fin du param&egrave;tre:
ex: <blockquote><?php echo htmlentities("<?php eval(\$plxShow->callHook('champArt', 'balise_L')); ?>"); ?></blockquote>
Cela affichera:
<blockquote>
Intitulé du champ personalisé: Valeur enregistrée dans le champ.
</blockquote>
</p>

</div>