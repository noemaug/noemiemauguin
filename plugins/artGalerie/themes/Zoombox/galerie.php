<?php if(!defined('PLX_ROOT')) exit; ?>

<div class="zoombox-thumbnails">
	<?php	foreach($galerie as $galImg) {?>
	
	
		<a title="<?php echo $galImg['title']; ?>" class="zoombox zgallery<?php echo $randstr; ?>" href="<?php echo $galImg['file']; ?>">
			<img class="lazy" data-original="<?php echo $galImg['file']; ?>" alt="<?php echo $galImg['alt']; ?>" src="<?php echo $galImg['file']; ?>" />
		</a>
		<!-- <div class="description"><?php echo $galImg['title']; ?></div> -->
	
	
	<?php	}?>
	<div style="clear:left;"></div>
</div>
